﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DebugInfoView : MonoBehaviour
{
    [SerializeField]
    private Text bodyCount;

    public string BodyCount
    {
        get { return bodyCount.text; }
        set
        {
            bodyCount.text = value;
        }
    }
}
