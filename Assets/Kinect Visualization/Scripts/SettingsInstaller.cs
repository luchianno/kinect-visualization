using UnityEngine;
using Zenject;

[CreateAssetMenu(fileName = "SettingsInstaller", menuName = "Installers/SettingsInstaller")]
public class SettingsInstaller : ScriptableObjectInstaller<SettingsInstaller>
{
    public Settings Settings;
    public override void InstallBindings()
    {
        Container.BindInstance<Settings>(Settings);
    }
}