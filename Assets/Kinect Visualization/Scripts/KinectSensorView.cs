﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class KinectSensorView : MonoBehaviour
{
	public Image BodyIndex;
	public Image Depth;
}
