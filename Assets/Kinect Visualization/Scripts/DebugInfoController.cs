﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;
using Windows.Kinect;
using System.Linq;

public class DebugInfoController : ITickable
{
    [Inject]
    IKinectManager kinect;
    [Inject]
    DebugInfoView view;

    public void Tick()
    {
        if (kinect.Bodies != null)
        {
            //            Debug.Log(kinect.Bodies[0] == null);
            view.BodyCount = $"Body Count: {kinect.Bodies.Count(x => x != null && x.IsTracked)}";
            //   Debug.Log(kinect.Bodies[0]);

        }
    }
}
