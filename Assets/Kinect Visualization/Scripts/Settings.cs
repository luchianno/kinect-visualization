﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class Settings : ScriptableObject
{
    [Tooltip("Must be 6 in length")]
    public Color32[] PlayerColors = new Color32[]{
        Color.red,
        Color.green,
        Color.blue,
        Color.yellow,
        Color.grey,
        Color.cyan};
}
