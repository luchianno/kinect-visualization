﻿using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using UnityEngine;
using Windows.Kinect;

public interface IKinectManager
{
    FrameDescription BodyIndexFrameDescription { get; }

    Texture2D BodyIndexTexture { get; }

    Color32[] ColorArray { get; }
    ReadOnlyCollection<Body> Bodies { get; }
}
