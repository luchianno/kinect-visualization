﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class KinectViewController : IInitializable
{
    [Inject]
    KinectSensorView view;

    [Inject]
    IKinectManager kinect;

    public void Initialize()
    {
        Debug.Log(kinect.BodyIndexTexture == null);
        var temp = Sprite.Create(kinect.BodyIndexTexture,
                                     new Rect(0, 0, kinect.BodyIndexTexture.width, kinect.BodyIndexTexture.height),
                                     new Vector2(0.5f, 0.5f));
        view.BodyIndex.sprite = temp;
        view.BodyIndex.preserveAspect = true;
    }
}
