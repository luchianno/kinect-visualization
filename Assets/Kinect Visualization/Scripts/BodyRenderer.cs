﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;
using Windows.Kinect;
using System;

public class BodyRenderer : MonoBehaviour
{
    [SerializeField]
    List<LineRenderer> renderers;
    [SerializeField]
    GameObject JointPrefab;

    [Inject]
    IKinectManager kinect;
    Dictionary<JointType, GameObject> Joints = new Dictionary<JointType, GameObject>();

    void Start()
    {
        foreach (JointType item in Enum.GetValues(typeof(JointType)))
        {
            var temp = Instantiate(JointPrefab, Vector3.zero, Quaternion.identity, this.transform);
            temp.name = item.ToString();
            Joints.Add(item, temp);
        }
    }

    void Update()
    {
        // Debug.Log("dsfdsf");
        foreach (var body in kinect.Bodies)
        {
            RenderBody(body);
        }
    }

    void OnDrawGizmos()
    {
        Gizmos.color = Color.black;
        if (kinect != null)
        {
            // Debug.Log("bodies loop");
            foreach (var body in kinect.Bodies)
            {
                if (body != null)
                {
                    foreach (var item in body.Joints)
                    {
                        // Debug.Log("dsdf");
                        Gizmos.DrawCube(ToVector3(item.Value.Position), new Vector3(0.1f, 0.1f, 0.1f));
                    }
                }
            }
        }
    }

    public void RenderBody(Body body)
    {
        if (kinect != null)
        {
            // Debug.Log("bodies loop");
            if (body != null && body.IsTracked)
            {
                foreach (var item in body.Joints)
                {
                    // Debug.Log("setting pos");
                    Joints[item.Key].transform.position = ToVector3(item.Value.Position);
                }
            }
        }
    }

    public static Vector3 ToVector3(CameraSpacePoint point)
    {
        return new Vector3(point.X, point.Y, point.Z);
    }
}
