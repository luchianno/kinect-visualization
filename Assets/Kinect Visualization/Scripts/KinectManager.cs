﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.UI;
using Windows.Kinect;
using Zenject;

public class KinectManager : IKinectManager, IInitializable, IDisposable
{
    public Texture2D BodyIndexTexture { get; protected set; }
    public FrameDescription BodyIndexFrameDescription { get; protected set; }
    public ReadOnlyCollection<Body> Bodies { get { return bodies.AsReadOnly(); } }
    public Color32[] ColorArray { get; protected set; }

    [Inject]
    Settings Options;

    Color32 EmptyColor = new Color(0f, 0f, 0f, 0f);

    KinectSensor sensor;
    BodyIndexFrameReader bodyIndexFrameReader;
    BodyFrameReader bodyFrameReader;

    FrameDescription depthFrameDescription;
    List<Body> bodies= new List<Body>(new Body[6]);

    const int BodyCount = 6;

    public void Initialize()
    {
        Debug.Log("Starting kinect manager");
        sensor = KinectSensor.GetDefault();
        sensor.IsAvailableChanged += Sensor_IsAvailableChanged;
        
        BodyIndexFrameDescription = sensor.BodyIndexFrameSource.FrameDescription;
        bodyIndexFrameReader = sensor.BodyIndexFrameSource.OpenReader();
        bodyFrameReader = sensor.BodyFrameSource.OpenReader();

        bodyIndexFrameReader.FrameArrived += BodyIndexFrameArrived;
        bodyFrameReader.FrameArrived += BodyFrameArrived;

        // allocate space to put the pixels being converted
        ColorArray = new Color32[this.BodyIndexFrameDescription.Width * this.BodyIndexFrameDescription.Height];
        BodyIndexTexture = new Texture2D(BodyIndexFrameDescription.Width, BodyIndexFrameDescription.Height,
                                        TextureFormat.RGBA32,
                                        false);

        // Debug.Log($"Width: {BodyIndexFrameDescription.Width}, Height: {BodyIndexFrameDescription.Height}");

        sensor.Open();
    }

    private void BodyFrameArrived(object sender, BodyFrameArrivedEventArgs e)
    {
        // bool dataReceived = false;

        using (BodyFrame bodyFrame = e.FrameReference.AcquireFrame())
        {

            if (bodyFrame != null)
            {
                bodyFrame.GetAndRefreshBodyData(this.bodies); // WTf with this performance, thanks obama
                //dataReceived = true;
            }
        }
    }

    // TODO add actual error handling maybe
    private void Sensor_IsAvailableChanged(object sender, IsAvailableChangedEventArgs e)
    {
        Debug.Log($"Kinect Sensor Available: {e.IsAvailable}");
    }

    private void BodyIndexFrameArrived(object sender, BodyIndexFrameArrivedEventArgs e)
    {
        var bodyIndexFrameProcessed = false;
        using (var bodyIndexFrame = e.FrameReference.AcquireFrame())
        {
            if (bodyIndexFrame != null)
            {
                // the fastest way to process the body index data is to directly access 
                // the underlying buffer
                using (KinectBuffer bodyIndexBuffer = bodyIndexFrame.LockImageBuffer())
                {
                    // verify data and write the color data to the display bitmap
                    if (((this.BodyIndexFrameDescription.Width * this.BodyIndexFrameDescription.Height) == bodyIndexBuffer.Length) &&
                        (this.BodyIndexFrameDescription.Width == this.BodyIndexTexture.width) && (this.BodyIndexFrameDescription.Height == this.BodyIndexTexture.height))
                    {
                        ProcessBodyIndexFrameData(bodyIndexBuffer.UnderlyingBuffer, bodyIndexBuffer.Length);
                        bodyIndexFrameProcessed = true;
                    }
                }
            }
        }

        if (bodyIndexFrameProcessed)
        {
            BodyIndexTexture.SetPixels32(ColorArray);
            BodyIndexTexture.Apply();
        }
    }

    // This function requires the /unsafe compiler option as we make use of direct
    // access to the native memory pointed to by the bodyIndexFrameData pointer.
    private unsafe void ProcessBodyIndexFrameData(IntPtr bodyIndexFrameData, uint bodyIndexFrameDataSize)
    {
        byte* frameData = (byte*)bodyIndexFrameData;
        bool detected = false;
        // convert body index to a visual representation
        for (int i = 0; i < (int)bodyIndexFrameDataSize; ++i)
        {
            // the BodyColor array has been sized to match
            // BodyFrameSource.BodyCount
            if (frameData[i] < BodyCount)
            {
                // this pixel is part of a player,
                // display the appropriate color
                // this.bodyIndexPixels[i] = BodyColor[frameData[i]];
                //this.foo.Ints[i] = BodyColor[frameData[i]];
                detected = true;
                this.ColorArray[i] = Options.PlayerColors[frameData[i]];
            }
            else
            {
                // this pixel is not part of a player
                // display black
                // this.bodyIndexPixels[i] = 0x00000000;
                //this.foo.Ints[i] = 0x00000000;
                this.ColorArray[i] = EmptyColor;
            }
        }
        // if (detected)
        //     Debug.Log("body detected");
    }

    public void Dispose()
    {
        Debug.Log("Disposing");
        if (this.bodyIndexFrameReader != null)
        {
            this.bodyIndexFrameReader.FrameArrived -= this.BodyIndexFrameArrived;

            this.bodyIndexFrameReader.Dispose();
            this.bodyIndexFrameReader = null;
        }

        if (this.bodyFrameReader != null)
        {
            this.bodyFrameReader.FrameArrived -= this.BodyFrameArrived;

            this.bodyFrameReader.Dispose();
            this.bodyFrameReader = null;
        }

        if (this.sensor != null)
        {
            this.sensor.Close();
            this.sensor = null;
        }
    }

}

