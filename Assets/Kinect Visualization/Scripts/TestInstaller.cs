using UnityEngine;
using Zenject;

public class TestInstaller : MonoInstaller<TestInstaller>
{
    [SerializeField]
    KinectSensorView view;
    [SerializeField]
    DebugInfoView debugInfoView;
    [SerializeField]
    Settings currentSettings;
    [SerializeField]
    BodyRenderer bodyRenderer;

    public override void InstallBindings()
    {
        Container.Bind<Settings>().FromInstance(currentSettings).AsSingle();

        Container.BindInterfacesTo<KinectManager>().AsSingle();

        // views
        Container.Bind<KinectSensorView>().FromInstance(view).AsSingle();
        Container.Bind<DebugInfoView>().FromInstance(debugInfoView).AsSingle();

        Container.BindInterfacesAndSelfTo<DebugInfoController>().AsSingle();

        Container.BindInterfacesTo<KinectViewController>().AsSingle();
        Container.Bind<BodyRenderer>().AsSingle();
        // Debug.Log("asdassdda");
    }
}